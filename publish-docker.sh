set -e

./gradlew bootBuildImage
docker login registry.gitlab.com -u ${GITLAB_DEPLOY_USER} -p ${GITLAB_DEPLOY_TOKEN}
docker push ${ECI_DOCKER_REGISTRY}/$(./gradlew properties | grep ^name | awk -F'name: ' '{print $2}'):$(./gradlew cV -q -Prelease.quiet)
