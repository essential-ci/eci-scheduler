package pl.jdudycz.eci.scheduler.agent.client

import pl.jdudycz.eci.scheduler.agent.client.model.JobsCountResponse
import pl.jdudycz.eci.scheduler.job.model.Job
import reactor.core.publisher.Mono

interface AgentClient {

    fun scheduleJob(agentHost: String, apiKey: String, job: Job): Mono<Void>

    fun getJobsCount(agentHost: String, apiKey: String): Mono<JobsCountResponse>

    fun cancelJob(agentHost: String, jobId: String): Mono<Void>
}
