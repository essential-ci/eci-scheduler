package pl.jdudycz.eci.scheduler.agent.client.model

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("pl.jdudycz.eci.scheduler.agents")
data class AgentsProperties(val port: Int, val protocol: String)
