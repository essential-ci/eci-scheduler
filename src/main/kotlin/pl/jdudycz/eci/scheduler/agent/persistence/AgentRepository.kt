package pl.jdudycz.eci.scheduler.agent.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface AgentRepository : ReactiveCrudRepository<Agent, String>{
    fun findBySpaceId(spaceId: String): Flux<Agent>
}
