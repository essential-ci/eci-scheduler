package pl.jdudycz.eci.scheduler.agent.client

import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.InsecureTrustManagerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

@Configuration
class HttpClientConfig {

    @Bean
    fun httpClient(): WebClient {
        val sslContext = SslContextBuilder
                .forClient()
                // TODO load issuer ca instead
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build()
        return HttpClient
                .create()
                .secure { it.sslContext(sslContext) }
                .let(::ReactorClientHttpConnector)
                .let { WebClient.builder().clientConnector(it).build() }
    }
}
