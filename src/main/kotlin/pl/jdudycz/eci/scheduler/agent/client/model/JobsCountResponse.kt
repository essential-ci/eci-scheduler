package pl.jdudycz.eci.scheduler.agent.client.model

data class JobsCountResponse(val queuedCount: Int, val runningCount: Int)
