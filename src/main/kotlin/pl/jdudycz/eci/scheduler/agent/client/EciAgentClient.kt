package pl.jdudycz.eci.scheduler.agent.client

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters.fromValue
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import pl.jdudycz.eci.scheduler.agent.client.model.AgentsProperties
import pl.jdudycz.eci.scheduler.agent.client.model.JobsCountResponse
import pl.jdudycz.eci.scheduler.job.model.Job
import reactor.core.publisher.Mono

@Service
class EciAgentClient(private val httpClient: WebClient,
                     private val properties: AgentsProperties) : AgentClient {

    override fun scheduleJob(agentHost: String, apiKey: String, job: Job): Mono<Void> =
            httpClient.post()
                    .uri(baseUrl(agentHost))
                    .body(fromValue(job))
                    .header(AGENT_API_KEY_HEADER, apiKey)
                    .exchange()
                    .doOnNext { it.throwOnUnsuccessful() }
                    .then()

    override fun cancelJob(agentHost: String, jobId: String): Mono<Void> =
            httpClient.post()
                    .uri("${baseUrl(agentHost)}/${jobId}/cancel")
                    .exchange()
                    .doOnNext { it.throwOnUnsuccessful() }
                    .then()

    override fun getJobsCount(agentHost: String, apiKey: String): Mono<JobsCountResponse> =
            httpClient.get()
                    .uri("${baseUrl(agentHost)}/count")
                    .header(AGENT_API_KEY_HEADER, apiKey)
                    .exchange()
                    .flatMap(::getBody)

    private fun getBody(response: ClientResponse): Mono<JobsCountResponse> =
            response.bodyToMono<JobsCountResponse>().doOnNext { response.throwOnUnsuccessful() }


    private fun ClientResponse.throwOnUnsuccessful() {
        if (!statusCode().is2xxSuccessful) throw Exception("Agent request failed with code ${rawStatusCode()}")
    }

    private fun baseUrl(agentHost: String) = "${properties.protocol}://${agentHost}:${properties.port}/${AGENT_JOB_ENDPOINT}"

    companion object {
        private const val AGENT_JOB_ENDPOINT = "api/job"
        private const val AGENT_API_KEY_HEADER = "Agent-Api-Key"
    }
}
