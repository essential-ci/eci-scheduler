package pl.jdudycz.eci.scheduler.job.model

import pl.jdudycz.eci.common.domain.Job
import pl.jdudycz.eci.common.util.asEnvMap

data class Task(val id: String,
                val ordinal: Int,
                val name: String,
                val image: String,
                val variables: Map<String, Any>,
                val secretVariables: Map<String, Any>,
                val commands: List<String>) {

    constructor(data: Job.PipelineTask, decryptionProvider: (String) -> String) : this(
            data.id,
            data.ordinal,
            data.name,
            data.image,
            data.variablesList.asEnvMap(),
            data.secretVariablesList.asEnvMap().mapValues { decryptionProvider(it.value.toString()) },
            data.commandsList
    )
}
