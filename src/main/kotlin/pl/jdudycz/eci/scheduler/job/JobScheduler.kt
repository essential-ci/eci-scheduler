package pl.jdudycz.eci.scheduler.job

import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.agentAvailable
import pl.jdudycz.eci.common.domain.event.agentUnavailable
import pl.jdudycz.eci.common.domain.event.jobFailed
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.scheduler.agent.client.AgentClient
import pl.jdudycz.eci.scheduler.agent.client.model.JobsCountResponse
import pl.jdudycz.eci.scheduler.agent.persistence.Agent
import pl.jdudycz.eci.scheduler.agent.persistence.AgentRepository
import pl.jdudycz.eci.scheduler.job.model.AgentJobState
import pl.jdudycz.eci.scheduler.job.model.Job
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class JobScheduler(private val agentRepository: AgentRepository,
                   private val agentClient: AgentClient,
                   private val eventPublisher: EventPublisher) {

    fun schedule(job: Job): Mono<String> =
            agentRepository
                    .findBySpaceId(job.spaceId)
                    .flatMap(::getState)
                    .switchIfEmpty(Mono.error(Exception("No available agents found")))
                    .collectList()
                    .map(::selectBestCandidate)
                    .flatMap { agentClient.scheduleJob(it.host, it.apiKey, job).then(it.id.toMono()) }
                    .doOnSuccess { log.debug("Job ${job.id} scheduled to agent $it") }
                    .onErrorResume { publishFailedEvent(job.id, "Could not schedule job: ${it.localizedMessage}") }

    private fun getState(agent: Agent): Mono<AgentJobState> =
            agentClient
                    .getJobsCount(agent.host, agent.apiKey)
                    .flatMap { if (!agent.isAvailable) markAvailable(agent).then(asState(agent, it)) else asState(agent, it) }
                    .onErrorResume { if (agent.isAvailable) markUnavailable(agent) else Mono.empty() }

    private fun asState(agent: Agent, response: JobsCountResponse): Mono<AgentJobState> =
            Mono.fromCallable { AgentJobState(agent, response.queuedCount, response.runningCount) }

    private fun markAvailable(agent: Agent): Mono<AgentJobState> = publish(agentAvailable(agent.id))

    private fun markUnavailable(agent: Agent): Mono<AgentJobState> = publish(agentUnavailable(agent.id))

    private fun publish(event: EventOuterClass.Event): Mono<AgentJobState> =
            Mono.fromCallable { event }.transform(eventPublisher::publish).then(Mono.empty())

    private fun selectBestCandidate(agentStates: List<AgentJobState>): Agent =
            agentStates.minByOrNull { it.queuedCount + it.runningCount * 0.5 }!!.agent

    private fun publishFailedEvent(jobId: String, errorMessage: String): Mono<String> =
            Mono.fromCallable { jobFailed(jobId, errorMessage) }
                    .transform(eventPublisher::publish)
                    .then(Mono.empty())

    companion object {
        private val log = logger<JobScheduler>()
    }
}
