package pl.jdudycz.eci.scheduler.job.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.JOB_CANCEL_TOPIC
import pl.jdudycz.eci.common.kafka.JOB_CREATE_SUCCESS_TOPIC
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class JobConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun jobCreateSuccess(): EventConsumer = EventConsumer.create(JOB_CREATE_SUCCESS_TOPIC, kafkaProperties)

    @Bean
    fun jobCancel(): EventConsumer = EventConsumer.create(JOB_CANCEL_TOPIC, kafkaProperties)
}
