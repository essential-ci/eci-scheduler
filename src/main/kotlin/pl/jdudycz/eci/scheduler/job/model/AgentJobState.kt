package pl.jdudycz.eci.scheduler.job.model

import pl.jdudycz.eci.scheduler.agent.persistence.Agent

data class AgentJobState(val agent: Agent, val queuedCount: Int, val runningCount: Int)
