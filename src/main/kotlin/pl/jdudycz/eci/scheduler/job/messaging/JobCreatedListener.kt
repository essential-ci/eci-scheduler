package pl.jdudycz.eci.scheduler.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job.JobData
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.encryption.TextEncryptor
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.scheduler.job.JobScheduler
import pl.jdudycz.eci.scheduler.job.model.Job
import javax.annotation.PostConstruct

@Service
class JobCreatedListener(@Qualifier("jobCreateSuccess") private val consumer: EventConsumer,
                         private val scheduler: JobScheduler,
                         private val encryptor: TextEncryptor) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { JobData.parseFrom(it.data) }
                .doOnNext { log.debug("Received job created event for job ${it.id}") }
                .map { Job(it, encryptor::decrypt) }
                .flatMap(scheduler::schedule)
                .subscribe()
    }

    companion object {
        private val log = logger<JobCreatedListener>()
    }
}
