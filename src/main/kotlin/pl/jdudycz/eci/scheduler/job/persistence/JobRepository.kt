package pl.jdudycz.eci.scheduler.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface JobRepository : ReactiveCrudRepository<JobEntity, String>
