package pl.jdudycz.eci.scheduler.job.persistence

import org.springframework.data.mongodb.core.mapping.Document

@Document
data class JobEntity(
        val id: String,
        val agentId: String?
)
