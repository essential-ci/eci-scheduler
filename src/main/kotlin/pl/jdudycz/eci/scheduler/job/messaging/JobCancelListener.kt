package pl.jdudycz.eci.scheduler.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.scheduler.agent.client.AgentClient
import pl.jdudycz.eci.scheduler.agent.persistence.Agent
import pl.jdudycz.eci.scheduler.agent.persistence.AgentRepository
import pl.jdudycz.eci.scheduler.job.persistence.JobRepository
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Service
class JobCancelListener(@Qualifier("jobCancel") private val consumer: EventConsumer,
                        private val agentRepository: AgentRepository,
                        private val jobRepository: JobRepository,
                        private val agentClient: AgentClient) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { it.data.toStringUtf8() }
                .flatMap(::cancelJob)
                .subscribe()
    }

    private fun cancelJob(jobId: String) =
            jobRepository.findById(jobId)
                    .flatMap {
                        if (it.agentId == null) {
                            Mono.error(Exception("Not assigned to any agent yet"))
                        } else agentRepository.findById(it.agentId)
                    }
                    .flatMap { agentClient.cancelJob(it.host, jobId) }
                    .doOnSuccess { log.debug("Job $jobId cancel request sent") }
                    .resumeOnError { log.debug("Could not cancel job $jobId: ${it.localizedMessage}")}

    companion object {
        private val log = logger<JobCancelListener>()
    }
}
