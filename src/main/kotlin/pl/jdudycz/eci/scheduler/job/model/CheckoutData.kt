package pl.jdudycz.eci.scheduler.job.model

import pl.jdudycz.eci.common.domain.Repo


data class CheckoutData(val ref: String,
                        val checkoutSha: String,
                        val repoUrl: String,
                        val token: String?) {

    constructor(change: Repo.ChangeData, repoToken: String?)
            : this(change.refValue, change.checkoutSha, change.repoUrl, repoToken)
}
