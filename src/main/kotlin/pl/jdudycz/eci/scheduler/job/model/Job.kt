package pl.jdudycz.eci.scheduler.job.model

import pl.jdudycz.eci.common.domain.Job

data class Job(
        val id: String,
        val spaceId: String,
        val tasks: List<Task>,
        val checkoutData: CheckoutData
) {

    constructor(data: Job.JobData, decryptionProvider: (String) -> String) : this(
            data.id,
            data.project.spaceId,
            data.tasksList.map { Task(it, decryptionProvider) },
            CheckoutData(
                    data.change,
                    if (data.project.token.isNotEmpty()) decryptionProvider(data.project.token) else null
            ),
    )
}
