package pl.jdudycz.eci.scheduler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import pl.jdudycz.eci.scheduler.agent.client.model.AgentsProperties

@SpringBootApplication
@EnableConfigurationProperties(AgentsProperties::class, KafkaProperties::class)
class Main

fun main(args: Array<String>) {
    runApplication<Main>(*args)
}
